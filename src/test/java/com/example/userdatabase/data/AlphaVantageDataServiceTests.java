package com.example.userdatabase.data;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

public class AlphaVantageDataServiceTests {

    private AlphaVantageDataService alphaVantageDataService;

    @Before
    public void setUp_validApiKeyHostUrl() {
        alphaVantageDataService = new AlphaVantageDataService();
        ReflectionTestUtils.setField(alphaVantageDataService, "HOST", "alpha-vantage.p.rapidapi.com");
        ReflectionTestUtils.setField(alphaVantageDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7e");
    }

    @Before
    public void setUp_invalidApiKey() {
        alphaVantageDataService = new AlphaVantageDataService();
        ReflectionTestUtils.setField(alphaVantageDataService, "HOST", "alpha-vantage.p.rapidapi.com");
        ReflectionTestUtils.setField(alphaVantageDataService, "API_KEY", "3b68e4eb3fmshe03cf9ab6c3f934p1f100bjsn1c86ac052a7");
    }

    @Test
    public void getUrl_validHostParameters() {
        setUp_validApiKeyHostUrl();
        String url = alphaVantageDataService.getUrl("SGD", "USD", "FX_WEEKLY");
        String expected = "https://alpha-vantage.p.rapidapi.com/query?datatype=json&from_symbol=SGD&to_symbol=USD&function=FX_WEEKLY";
        JSONObject json = alphaVantageDataService.getData(expected);
        assertAll(
                () -> assertEquals(url, expected),
                () -> assertNotNull(json)
        );
    }

    @Test
    public void getData_invalidAPI() {
        setUp_invalidApiKey();
        JSONObject actual = alphaVantageDataService.getDailyDataFor("SGD", "USD");
        JSONObject expected = new JSONObject();
        expected.put("message", "You are not subscribed to this API.");
        assertEquals(expected.get("message"), actual.get("message"));
    }

    @Test
    public void getData_invalidCurrency() {
        setUp_validApiKeyHostUrl();
        String url = alphaVantageDataService.getUrl("SGX", "USD", "FX_WEEKLY");
        JSONObject json = alphaVantageDataService.getData(url);
        assertNotNull(json.get("Error Message"));
    }

}
