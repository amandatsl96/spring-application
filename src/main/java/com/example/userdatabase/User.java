package com.example.userdatabase;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

@Document
public class User {

    @Id
    private String id;
    private String name;
    private String email;
    private String number;
    private String apiKey = String.valueOf(generateSalt());

    // static variable to generate apiKey for market data access
    private static int ITERATIONS = 10;

    // static variable to generate apiKey for market data access
    private static int KEY_LENGTH = 32;

    public User() {
    }

    public User(String name, String email, String number) {
        this.name = name;
        this.email = email;
        this.number = number;
        char[] uniqueString = this.email.concat(this.number).toCharArray();
        this.apiKey = String.valueOf(hash(uniqueString));
    }

    //Getters
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNumber() {
        return number;
    }

    public String getApiKey() {
        return apiKey;
    }

    //Setters
    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * to generate salt for apiKey construction for market data access
     * @return byte[]
     */
    private static byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[32];
        random.nextBytes(bytes);
        return bytes;
    }

    /**
     * to generate apiKey for market data access
     * @param uniqueString
     * @return byte[]
     */
    private static byte[] hash(char[] uniqueString) {
        byte[] salt = generateSalt();
        PBEKeySpec spec = new PBEKeySpec(uniqueString, salt, ITERATIONS, KEY_LENGTH);
        Arrays.fill(uniqueString, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Error while hashing: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }
}
