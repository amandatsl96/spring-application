package com.example.userdatabase.data;

import com.example.userdatabase.UserRepository;
import com.example.userdatabase.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * This is a REST controller for querying stock & market data for Task 3
 * @author anna
 */
@RestController
@CrossOrigin
@RequestMapping("/data")
public class QueryDataController {

    @Autowired
    QueryDataService queryDataService;

    @Autowired
    UserService userService;

    /**
     * Wrapper function for authentication
     * If changes are made to userRepository or the authentication method, please make the changes here!
     * @param apiKey
     * @return
     */
    private boolean authenticateUser(String apiKey) {
        //return true;
        return userService.authenticateApiKey(apiKey);
    }

    /**
     * Wrapper function for functions
     * @param apiKey
     * @param function
     * @return ResponseEntity
     */
    ResponseEntity getRequestWrapper(String apiKey, Supplier<String> function) {
        if (authenticateUser(apiKey)) {
            String response = function.get();
            return ResponseEntity.ok().body(response);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Gets the QUANDL Tickers for querying
     * This process is done by reading a csv file so please change the Path variable as appropriate!
     * @param API_KEY
     * @return ResponseEntity<List<String>>
     */
    @GetMapping("/stock/list")
    ResponseEntity<List<String>> getStockTickerList(@RequestHeader(value="API-KEY") String API_KEY) {
        if (authenticateUser(API_KEY)) {
            List<String> tickers = queryDataService.getAllTickers();
            return ResponseEntity.ok().body(tickers);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Gets the QUANDL Metadata of a Stock, includes information such as latest/oldest update
     * @param ticker
     * @param API_KEY
     * @return ResponseEntity
     */
    @GetMapping(value = "/stock/{ticker}/meta", produces = "application/json")
    ResponseEntity getStockMetadata(@PathVariable String ticker, @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getStockMetadata(ticker).toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Gets the QUANDL Stock prices of a stock
     * @param ticker
     * @param start
     * @param end
     * @param API_KEY
     * @return ResponseEntity
     */
    @GetMapping(value = "/stock/{ticker}", produces = "application/json")
    ResponseEntity getStockDataForTime(@PathVariable String ticker,
                                       @RequestParam(name="startDate") String start,
                                       @RequestParam(name="endDate") String end,
                                       @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getPriceForTickerFilterByDate(ticker, start, end).toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Returns a list of the regions for querying further market data
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/market/list", produces = "application/json")
    ResponseEntity<List<String>> getMarketDataRegions(@RequestHeader(value="API-KEY") String API_KEY) {
        if (authenticateUser(API_KEY)) {
            List<String> regionList = queryDataService.getRegionList();
            return ResponseEntity.ok().body(regionList);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Gets the Summary of Market Data by regions from Yahoo
     * Note: This process is expensive so don't do it too much!!
     * @param region
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/market/{region}", produces = "application/json")
    ResponseEntity getMarketDataSummaryByRegion(@PathVariable String region,
                                                @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getMarketDataByRegion(region).toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Gets the Trending Market Data by regions from Yahoo
     * Note: This process is expensive so don't do it too much!!
     * @param region
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/market/{region}/trending", produces = "application/json")
    ResponseEntity getTrendingMarketDataByRegion(@PathVariable String region,
                                                 @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getTrendingMarketByRegion(region).toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Gets the Popular Watchlist data from Yahoo
     * Note: This process is expensive so don't do it too much!!
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/market/watchlist", produces = "application/json")
    ResponseEntity getWatchList(@RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getWatchList().toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Gets the list of News from Bloomberg
     * Note: This process is expensive so don't do it too much!!
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/news/list", produces = "application/json")
    ResponseEntity getNewsList(@RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getNewsList().toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Depreciated
     * Gets the available currencies from the Currency Converter
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/depreciated/list", produces = "application/json")
    ResponseEntity getCurrency(@RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getCurrencyList().toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Depreciated
     * Converts a currency to another using the Currency Converter in real time
     * @param curr1
     * @param curr2
     * @param amt
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/depreciated/realtime", produces = "application/json")
    ResponseEntity getRealCurrency(@RequestParam(name="from") String curr1,
                                   @RequestParam(name="to") String curr2,
                                   @RequestParam(name="amt") Long amt,
                                   @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getCurrencyConverter(curr1, curr2, amt).toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Depreciated
     * Returns a list of currency conversion rates for the date specified
     * @param date
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/depreciated/historic/{date}", produces = "application/json")
    ResponseEntity getHistoricCurrency(@PathVariable String date, @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> function = () -> queryDataService.getHistoricCurrencyConverter(date).toString(4);
        return getRequestWrapper(API_KEY, function);
    }

    /**
     * Get real time currency conversion rates from a currency and to another currency
     * @param curr1
     * @param curr2
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/currency/real", produces = "application/json")
    ResponseEntity getCurrencyConverter(@RequestParam(name="from") String curr1,
                                        @RequestParam(name="to") String curr2,
                                        @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> getDailyCurrency =  () -> queryDataService.getCurrencyConverterAlpha(curr1, curr2).toString(4);
        return getRequestWrapper(API_KEY, getDailyCurrency);
    }

    /**
     * Get monthly currency conversion rates from a currency and to another currency
     * @param curr1, curr2
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/currency/monthly", produces = "application/json")
    ResponseEntity getCurrencyMonthly(@RequestParam(name="from") String curr1,
                                      @RequestParam(name="to") String curr2,
                                      @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> getMonthlyCurrency =  () -> queryDataService.getMonthlyDataFor(curr1, curr2).toString(4);
        return getRequestWrapper(API_KEY, getMonthlyCurrency);
    }

    /**
     * Get weekly currency conversion rates from a currency and to another currency
     * @param curr1, curr2
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/currency/weekly", produces = "application/json")
    ResponseEntity getCurrencyWeekly(@RequestParam(name="from") String curr1,
                                     @RequestParam(name="to") String curr2,
                                     @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> getWeeklyCurrency =  () -> queryDataService.getWeeklyDataFor(curr1, curr2).toString(4);
        return getRequestWrapper(API_KEY, getWeeklyCurrency);
    }

    /**
     * Get daily currency conversion rates from a currency and to another currency
     * @param curr1, curr2
     * @param API_KEY
     * @return
     */
    @GetMapping(value = "/currency/daily", produces = "application/json")
    ResponseEntity getCurrencyDaily(@RequestParam(name="from") String curr1,
                                    @RequestParam(name="to") String curr2,
                                    @RequestHeader(value="API-KEY") String API_KEY) {
        Supplier<String> getDailyCurrency =  () -> queryDataService.getDailyDataFor(curr1, curr2).toString(4);
        return getRequestWrapper(API_KEY, getDailyCurrency);
    }

}
