package com.example.userdatabase.data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;


/**
 * This data service is depreciated! (as it keeps asking for me to pay...)
 * Data service for accessing Currency Converter
 * @author anna
 *
 */
@Service
@Lazy
public class CurrencyDataService {

    @Value("${rapidapi.apikey}")
    private String API_KEY;

    @Value("${rapidapi.host.currencyconverter}")
    private String HOST;

    public JSONObject getAllCurrencies() {
        String url = String.format("https://%s/currency/list?format=json", HOST);
        return getData(url);
    }

    public JSONObject getCurrencyConverterFromTo(String curr1, String curr2, Long amt) {
        String url = String.format("https://%s/currency/convert?format=json&from=%s&to=%s&amount=%d",
                HOST, curr1, curr2, amt);
        return getData(url);
    }

    public JSONObject getHistoricCurrencyConverterFor(String date) {
        String url = String.format("https://%s/currency/historical/%s", HOST, date);
        return getData(url);
    }

    private JSONObject getData(String url) {
        JSONObject res = new JSONObject();
        try {
            HttpResponse<JsonNode> response = Unirest.get(url)
                    .header("x-rapidapi-host", HOST)
                    .header("x-rapidapi-key", API_KEY)
                    .asJson();
            res = response.getBody().getObject();
        } catch (UnirestException ue) {
            System.out.println(ue.getMessage());
        }
        return res;
    }


}
